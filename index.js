const escapeHtml = require('escape-html');
var request = require('request');

/**
 * HTTP Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 *                     More info: https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context.
 *                     More info: https://expressjs.com/en/api.html#res
 */
exports.keywordSuggest = (req, res) => {
  var options = {
    method: 'GET',
    url: 'http://suggestqueries.google.com/complete/search',
    qs: {
      q: req.query.keyword,
      client: 'chrome',
      _: '1564399410786'
    },
    headers: {
      DNT: '1',
      'User-Agent':
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.81 Safari/537.36'
    }
  };

  request(options, function(error, response, body) {
    if (error) throw new Error(error);
    res.set('Access-Control-Allow-Origin', '*');
    res.send({ success: true });
  });
};
